== About ==
Drush dpkg is a very simple drush command that simply invokes 
module and command hooks at various stages of the installation 
process of a Debian package.  These hooks are merely semantic 
untill they are placed within debian postinst, preinst, postrm, 
prerm and config scripts prior to compiling a debian package.

The overall goal is to expose Drupal to various stages of a 
debian package installation process so modules may interact and 
preform tasks at the different stages of a debian package.

Basic examples are included in the module.
