<?php

/**
 * Implementation of hook_drush_command().
 */
function dpkg_drush_command() {
  $items['dpkg-preinst'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'description' => 'Execute drupal callbacks on a preinst phase of a dpkg installation process.',
    'arguments' => array('pkg' => '', 'op' => ''),
    'examples' => array(
      'dpkg-preinst upgrade' => 'Run preinst hooks that respond to the upgrade call.',
    ),
  );

  $items['dpkg-postinst'] = array(
    'description' => 'Execute drupal callbacks on a postinst phase of a dpkg installation process.',
    'arguments' => array('pkg' => '', 'op' => ''),
    'examples' => array(
      'dpkg-postinst upgrade' => 'Run postinst hooks that respond to the upgrade call.',
    ),
  );

  $items['dpkg-prerm'] = array(
    'description' => 'Execute drupal callbacks on a prerm phase of a dpkg installation process.',
    'arguments' => array('pkg' => '', 'op' => ''),
    'examples' => array(
      'dpkg-prerm upgrade' => 'Run prerm hooks that respond to the upgrade call.',
    ),
  );

  $items['dpkg-postrm'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'description' => 'Execute drupal callbacks on a postrm phase of a dpkg installation process.',
    'arguments' => array('pkg' => '', 'op' => ''),
    'examples' => array(
      'dpkg-postrm upgrade' => 'Run postrm hooks that respond to the upgrade call.',
    ),
  );

  $items['dpkg-config'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'description' => 'Execute drupal callbacks on a config phase of a dpkg installation process.',
    'arguments' => array('pkg' => ''),
    'examples' => array(
      'dpkg-config' => 'Run config hooks',
    ),
  );

  return $items;
}

/**
 * Drush callback: dpkg-preinst.
 */
function drush_dpkg_preinst($pkg, $op) {
  drush_log("Running dpkg preinst: $op", 'status');
  drush_command_invoke_all('dpkg_preinst', $op, $pkg);
  drush_bootstrap_to_phase(DRUSH_BOOTSTRAP_DRUPAL_FULL);
  if (drush_get_context('DRUSH_BOOTSTRAP_PHASE') == DRUSH_BOOTSTRAP_DRUPAL_FULL) {
    module_invoke_all('dpkg_preinst', $op, $pkg);
  }
}

/**
 * Drush callback: dpkg-postinst.
 */
function drush_dpkg_postinst($pkg, $op) {
  drush_log("Running dpkg postinst: $op", 'status');
  drush_command_invoke_all('dpkg_postinst', $op, $pkg);
  drush_bootstrap_to_phase(DRUSH_BOOTSTRAP_DRUPAL_FULL);
  if (drush_get_context('DRUSH_BOOTSTRAP_PHASE') == DRUSH_BOOTSTRAP_DRUPAL_FULL) {
    module_invoke_all('dpkg_postinst', $op, $pkg);
  }
}

/**
 * Drush callback: dpkg-prerm.
 */
function drush_dpkg_prerm($pkg, $op) {
  drush_log("Running dpkg prerm: $op", 'status');
  drush_command_invoke_all('dpkg_prerm', $op, $pkg);
  drush_bootstrap_to_phase(DRUSH_BOOTSTRAP_DRUPAL_FULL);
  if (drush_get_context('DRUSH_BOOTSTRAP_PHASE') == DRUSH_BOOTSTRAP_DRUPAL_FULL) {
    module_invoke_all('dpkg_prerm', $op, $pkg);
  }
}

/**
 * Drush callback: dpkg-postrm.
 */
function drush_dpkg_postrm($pkg, $op) {
  drush_log("Running dpkg postrm: $op", 'status');
  drush_command_invoke_all('dpkg_postrm', $op, $pkg);
  drush_bootstrap_to_phase(DRUSH_BOOTSTRAP_DRUPAL_FULL);
  if (drush_get_context('DRUSH_BOOTSTRAP_PHASE') == DRUSH_BOOTSTRAP_DRUPAL_FULL) {
    module_invoke_all('dpkg_postrm', $op, $pkg);
  }
}

/**
 * Drush callback: dpkg-config.
 */
function drush_dpkg_config($pkg) {
  drush_log("Running dpkg config", 'status');
  drush_command_invoke_all('dpkg_config', $pkg);
  drush_bootstrap_to_phase(DRUSH_BOOTSTRAP_DRUPAL_FULL);
  if (drush_get_context('DRUSH_BOOTSTRAP_PHASE') == DRUSH_BOOTSTRAP_DRUPAL_FULL) {
    module_invoke_all('dpkg_config', $pkg);
  }
}

