<?php

/**
 * @group Drupal module hooks.
 *
 * These hooks are only ever invoked if drush can bootstrap drupal.
 */

/**
 * Implementation of hook_dpkg_config().
 *
 * @param debian package name being configured.
 */
function hook_dpkg_config($pkg) {
}

/**
 * Implementation of hook_dpkg_preinst().
 *
 * @param debian package name being configured.
 * @param operation. possible values: "install", "upgrade" or "abort-upgrade".
 */
function hook_dpkg_preinst($pkg, $op) {
}

/**
 * Implementation of hook_dpkg_postinst().
 *
 * @param debian package name being configured.
 * @param operation. possible values: "configure", "abort-upgrade", "abort-remove" or "abort-deconfigure".
 */
function hook_dpkg_postinst($pkg, $op) {
}

/**
 * Implementation of hook_dpkg_prerm().
 *
 * @param debian package name being configured.
 * @param operation. possible values: "remove", "upgrade", "deconfigure" or "failed-upgrade".
 */
function hook_dpkg_prerm($pkg, $op) {
}

/**
 * Implementation of hook_dpkg_postrm().
 *
 * @param debian package name being configured.
 * @param operation. possible values: "purge", "remove", "upgrade", "failed-upgrade", "abort-install", "abort-upgrade" or "disappear".
 */
function hook_dpkg_postrm($pkg, $op) {
}

/**
 * @endgroup Drupal module hooks.
 */
